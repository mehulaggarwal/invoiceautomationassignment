package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;

public class LoginPage {
	

	WebDriver driver;
	GenericFunction generic;
	public LoginPage(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
	}
	
	By login_email=By.id("user_login");
	By login_password=By.id("user_password");
	By submit=By.xpath("//input[@type='submit']");
	
	public void enterEmail(String email) {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(login_email));
		generic.Fill_Text(login_email, email);
	}
	
	public void enterPassword(String password) {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(login_password));
		generic.Fill_Text(login_password, password);
	}
	
	public void submit() {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(submit));
		generic.Click(submit);
	}

}
