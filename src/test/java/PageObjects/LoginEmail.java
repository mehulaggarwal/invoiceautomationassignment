package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;


public class LoginEmail {

	WebDriver driver;
	GenericFunction generic;
	WebDriverWait wait;

	public LoginEmail(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		wait = new WebDriverWait(driver, 10);
	}

	By emailText = By.id("identifierId");
	By signInButton = By.xpath("//*[@id=\"identifierNext\"]/span/span");

	public void enterEmail(String email) {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(emailText));
			generic.Fill_Text(emailText, email);
			generic.Click(signInButton);
		}
		catch(WebDriverException web) {
			System.out.println("Not on the sign in page");
		}
		
	}

}
