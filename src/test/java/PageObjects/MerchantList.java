package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;

public class MerchantList {

	WebDriver driver;
	GenericFunction generic;

	public MerchantList(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
	}

	By viewDashboard=By.xpath("//a[@href='https://pp32admin.payu.in/viewDashboard?mid=2&aid=32']");
	
	public void clickViewDashboard() {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(viewDashboard));
		generic.Click(viewDashboard);
	}
	
}
