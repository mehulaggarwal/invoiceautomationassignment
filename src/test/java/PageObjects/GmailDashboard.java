package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.GenericFunction;

public class GmailDashboard {

	WebDriver driver;
	GenericFunction generic;

	public GmailDashboard(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
	}

	By searchBox = By.xpath("//input[@placeholder='Search mail']");
	By searchButton = By.xpath("//button[@aria-label='Search Mail']");
	By email;

	public void searchIn(String word) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(searchBox));
		generic.Fill_Text(searchBox, word);
	}

	public void clickSearchButton() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(searchButton));
		generic.Click(searchButton);
	}

	public void searchBySubject(String search) {
		try {	
			String xpath="//div[@role='main'] //tbody/tr[@role='row'][";
			xpath=xpath+"contains(.,'"+search+"')]";
			email=By.xpath(xpath);
			WebDriverWait wait = new WebDriverWait(driver, 10);
			wait.until(ExpectedConditions.visibilityOfElementLocated(email));
			System.out.println("Email Found");
			Assert.assertTrue(true);
		} catch (Exception e) {
			Assert.assertTrue(true);
			System.out.println("Email Not found");
		}
	}
	
}
