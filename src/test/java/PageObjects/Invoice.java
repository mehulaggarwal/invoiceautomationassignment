package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;

public class Invoice {

	WebDriver driver;
	GenericFunction generic;

	public Invoice(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
	}

	By name_locator = By.xpath("//input[@name='firstname']");
	By email_locator = By.xpath("//input[@type='email']");
	By amount_locator = By.xpath("//input[@name='amount']");
	By mobile_locator = By.xpath("//input[@name='phone']");
	By transactionId_locator = By.xpath("//input[@name='txnid']");
	By description_locator = By.xpath("//textarea[@name='productinfo']");
	By submit_locator=By.xpath("//button[@type='submit'][contains(.,'CONFIRM')]");
	By template_locator=By.id("emailTemplateDdn");
	By sendInvoice_locator=By.xpath("//a[contains(.,'SEND INVOICE')]");
	By close_locator=By.xpath("//div[@class='right icon-font'][contains(.,'CLOSE')]");

	public void enterName(String name) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(name_locator));
		generic.Fill_Text(name_locator, name);
	}

	public void enterEmail(String email) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(email_locator));
		generic.Fill_Text(email_locator, email);
	}

	public void enterAmount(String amount) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(amount_locator));
		generic.Fill_Text(amount_locator, amount);
	}

	public void enterMobile(String mobile) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(mobile_locator));
		generic.Fill_Text(mobile_locator, mobile);
	}

	public void enterTransactionId(String transactioId) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(transactionId_locator));
		generic.Fill_Text(transactionId_locator, transactioId);
	}

	public void enterDescription(String description) {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(description_locator));
		generic.Fill_Text(description_locator, description);
	}
	
	public void clickSubmit() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(submit_locator));
		generic.Click(submit_locator);
	}
	
	public void selectEmailTemplate() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(template_locator));
		Select templates = new Select(driver.findElement(template_locator));
		templates.selectByIndex(1);
	}
	
	public void clickSendInvoice() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(sendInvoice_locator));
		generic.Click(sendInvoice_locator);
	}
	
	public void close() {
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(close_locator));
		generic.Click(close_locator);
	}
	
}
