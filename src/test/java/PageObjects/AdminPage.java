package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;

public class AdminPage {
	
	WebDriver driver;
	GenericFunction generic;
	
	
	
	public AdminPage(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
	}

	By loginWithHubLink=By.xpath("//div[@id='login-button']/a[contains(.,'Login With')]");
	By merchantLink=By.xpath("//a[@href='https://pp32admin.payu.in/merchantList']");
	
	public void loginWithHub() {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(loginWithHubLink));
		generic.Click(loginWithHubLink);
	}

	
	public void clickMerchantList() {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(merchantLink));
		generic.Click(merchantLink);
	}
	
}
