package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;

public class MerchantPanel {
	

	WebDriver driver;
	GenericFunction generic;
	
	
	public MerchantPanel(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
	}

	By newEmailInvoice=By.xpath("//strong[contains(.,'NEW EMAIL INVOICE')]");
	By myAccount=By.xpath("//span[contains(.,'My Account')]");
	By signOut_locator=By.xpath("//a[contains(.,'Sign Out')]");

	public void clicKNewEmailInvoice() {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(newEmailInvoice));
		generic.Click(newEmailInvoice);
	}
	
	public void signOut() {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(myAccount));
		generic.Click(myAccount);
		wait.until(ExpectedConditions.visibilityOfElementLocated(signOut_locator));
		generic.Click(signOut_locator);
	}

}
