package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import Utilities.GenericFunction;


public class LoginPassword {

	WebDriver driver;
	GenericFunction generic;
	WebDriverWait wait;

	

	public LoginPassword(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		wait = new WebDriverWait(driver, 10);
	}
	
	By passwordId = By.xpath("//input[@type='password']");
	By nextButton=By.xpath("//*[@id=\"passwordNext\"]/span/span");

	public void enterPassword(String password) {
		try {
			wait.until(ExpectedConditions.visibilityOfElementLocated(passwordId));
			generic.Fill_Text(passwordId, password);
			generic.Click(nextButton);
		}
		catch(WebDriverException webDriverException) {
			System.out.println("Not on the password Page");
		}
		
	}

}
