package PageObjects;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import Utilities.GenericFunction;

public class HomePage {
	
	WebDriver driver;
	GenericFunction generic;
	public HomePage(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
	}
	
	By adminLogin=By.xpath("//a[@href='https://pp32admin.payu.in/login']");
	
	public void clickAdminLogin() {
		WebDriverWait wait=new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.visibilityOfElementLocated(adminLogin));
		driver.findElement(adminLogin).click();
	}
	
}
