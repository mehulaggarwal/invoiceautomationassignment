package Utilities;

import org.openqa.selenium.WebDriver;

import PageObjects.Invoice;

public class InvoiceHelper {
	
	WebDriver driver;
	GenericFunction generic;
	Invoice invoice;
	static String txnId;
	
	public InvoiceHelper(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		invoice=new Invoice(driver, generic);
	}

	public void fillInvoice() {
		invoice.enterName("Mehul");
		invoice.enterEmail("mehulaggarwalpayu@gmail.com");
		invoice.enterAmount("123");
		invoice.enterMobile("8851795365");
		String transactionId=generic.getRandomString(10);
		txnId=transactionId;
		invoice.enterTransactionId(transactionId);
		invoice.enterDescription("This is just a test");
		invoice.clickSubmit();
	}
	
	public void sendInvoice() {
		invoice.selectEmailTemplate();
		invoice.clickSendInvoice();	
	}
	
	public void close() {
		invoice.close();
	}
	
	public static String getTransactionId() {
		return txnId;
	}
	
}
