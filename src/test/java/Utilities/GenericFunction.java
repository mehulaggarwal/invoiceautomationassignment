package Utilities;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class GenericFunction {

	WebDriver driver;
	String browserType;

	public GenericFunction(WebDriver driver) {
		super();
		this.driver = driver;
	}

	public WebDriver StartDriver(String browserType) {
		this.browserType = browserType;
		if (browserType.toLowerCase().equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			this.driver = new ChromeDriver();

		} else if (browserType.toLowerCase().equals("firefox")) {
			WebDriverManager.firefoxdriver().setup();
			this.driver = new FirefoxDriver();
		}
		return driver;
	}

	public boolean isVisible(By locator) {
		return driver.findElement(locator).isDisplayed();
	}

	public void Click(By locator) {
		driver.findElement(locator).click();
	}

	public void Fill_Text(By locator, String value) {
		WebElement element = driver.findElement(locator);
		element.sendKeys(value);
	}

	public void switchToNewWindow(String currentHandle) {
		Set<String> handles = driver.getWindowHandles();
		for (String actual : handles) {
			if (!actual.equalsIgnoreCase(currentHandle)) { 
				driver.switchTo().window(actual); 
				break;
			}
		}
	}

	public String getRandomString(int n) {

		String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ" + "0123456789" + "abcdefghijklmnopqrstuvxyz";

		StringBuilder sb = new StringBuilder(n);

		for (int i = 0; i < n; i++) {
			int index = (int) (AlphaNumericString.length() * Math.random());
			sb.append(AlphaNumericString.charAt(index));
		}

		return sb.toString();
	}

}
