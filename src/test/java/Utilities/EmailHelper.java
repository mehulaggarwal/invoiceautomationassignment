package Utilities;

import org.openqa.selenium.WebDriver;

public class EmailHelper {

	WebDriver driver;
	GenericFunction generic;
	GmailLoginHelper gmailLoginHelper;
	SearchHelper searchHelper;

	public EmailHelper(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		gmailLoginHelper = new GmailLoginHelper(driver, generic);
		searchHelper = new SearchHelper(driver, generic);
	}

	
	public void verifyEmail() {
		gmailLoginHelper.doLogin();
		searchHelper.searchIn("in:spam");
		searchHelper.searchBySubject("Your order at SMSPlus: Invoice #");
	}
	
}
