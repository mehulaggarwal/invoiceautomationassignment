package Utilities;

import org.openqa.selenium.WebDriver;

import PageObjects.AdminPage;

public class AdminPageHelper {

	WebDriver driver;
	GenericFunction generic;
	AdminPage adminPage;
	
	public AdminPageHelper(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		adminPage=new AdminPage(driver, generic);
		
	}
	
	public void loginWithHub() {
		adminPage.loginWithHub();
	}
	
	public void clickMerchantList() {
		adminPage.clickMerchantList();
	}
}
