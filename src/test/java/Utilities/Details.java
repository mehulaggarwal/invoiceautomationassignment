package Utilities;

import org.openqa.selenium.WebDriver;

public class Details {

	WebDriver driver;
	GenericFunction generic;
	HomePageHelper homePageHelper;
	AdminPageHelper adminPageHelper;
	LoginPageHelper loginPageHelper;
	MerchantListHelper merchantListHelper;
	String currentHandle;
	MerchantPanelHepler merchantPanelHepler;
	InvoiceHelper invoiceHelper;
	

	public Details(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		homePageHelper = new HomePageHelper(driver, generic);
		adminPageHelper = new AdminPageHelper(driver, generic);
		loginPageHelper = new LoginPageHelper(driver, generic);
		merchantListHelper = new MerchantListHelper(driver, generic);
		merchantPanelHepler = new MerchantPanelHepler(driver, generic);
		invoiceHelper = new InvoiceHelper(driver, generic);
		
	}
	
	public void startFilling() {
		homePageHelper.clickAdminLogin();
		adminPageHelper.loginWithHub();
		loginPageHelper.doLogin();
		currentHandle=driver.getWindowHandle();
        System.out.println();
		adminPageHelper.clickMerchantList();
		merchantListHelper.clickViewDashboard();
		generic.switchToNewWindow(currentHandle);
		merchantPanelHepler.clickNewEmailInvoice();
		invoiceHelper.fillInvoice();
		invoiceHelper.sendInvoice();
		invoiceHelper.close();
		merchantPanelHepler.signOut();
	}

}
