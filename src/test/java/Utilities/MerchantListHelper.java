package Utilities;

import org.openqa.selenium.WebDriver;

import PageObjects.MerchantList;

public class MerchantListHelper {

	WebDriver driver;
	GenericFunction generic;
	MerchantList merchantList;

	public MerchantListHelper(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		merchantList=new MerchantList(driver, generic);
	}
	
	public void clickViewDashboard() {
		merchantList.clickViewDashboard();
	}

}
