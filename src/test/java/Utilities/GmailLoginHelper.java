package Utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;

import PageObjects.LoginEmail;
import PageObjects.LoginPassword;

public class GmailLoginHelper {
	
	WebDriver driver;
	GenericFunction generic;
	LoginEmail loginEmail;
	LoginPassword loginPassword;
	
	public GmailLoginHelper(WebDriver driver,GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic=generic;
		loginEmail=new LoginEmail(driver, generic);
		loginPassword=new LoginPassword(driver, generic);
	}
	
	public void doLogin() {
		loginEmail.enterEmail("mehulaggarwalpayu@gmail.com");
		loginPassword.enterPassword("hellopayu");
	}

}
