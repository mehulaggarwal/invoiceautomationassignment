package Utilities;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;

import PageObjects.GmailDashboard;

public class SearchHelper {

	WebDriver driver;
	GenericFunction generic;
	GmailDashboard gmailDashboard;

	public SearchHelper(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		gmailDashboard = new GmailDashboard(driver, generic);
	}

	public void searchIn(String word) {
		gmailDashboard.searchIn(word);
		gmailDashboard.clickSearchButton();
	}

	public void searchBySubject(String subject) {
		driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		String txnId=InvoiceHelper.getTransactionId();
		gmailDashboard.searchBySubject(subject+txnId);
	}
	
}
