package Utilities;

import org.openqa.selenium.WebDriver;

import PageObjects.MerchantPanel;

public class MerchantPanelHepler {
	
	WebDriver driver;
	GenericFunction generic;
	MerchantPanel merchantPanel;
	
	public MerchantPanelHepler(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		merchantPanel=new MerchantPanel(driver, generic);
	}
	
	public void clickNewEmailInvoice() {
		merchantPanel.clicKNewEmailInvoice();
	}
	
	public void signOut() {
		merchantPanel.signOut();
	}

}
