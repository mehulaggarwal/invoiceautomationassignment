package Utilities;

import org.openqa.selenium.WebDriver;

import PageObjects.LoginPage;

public class LoginPageHelper {

	WebDriver driver;
	GenericFunction generic;
	LoginPage loginPage;
	
	public LoginPageHelper(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		loginPage=new LoginPage(driver, generic);
	}
	
	public void doLogin() {
		loginPage.enterEmail("qc2@gmail.com");
		loginPage.enterPassword("Payu@1234");
		loginPage.submit();
	}

}
