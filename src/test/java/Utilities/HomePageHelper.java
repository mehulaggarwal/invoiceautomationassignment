package Utilities;

import org.openqa.selenium.WebDriver;

import PageObjects.HomePage;

public class HomePageHelper {
	
	WebDriver driver;
	GenericFunction generic;
	HomePage homePage;
	
	public HomePageHelper(WebDriver driver, GenericFunction generic) {
		super();
		this.driver = driver;
		this.generic = generic;
		homePage=new HomePage(driver, generic);
	}
	
	public void clickAdminLogin() {
		homePage.clickAdminLogin();
	}
}
