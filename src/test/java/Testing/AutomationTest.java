package Testing;



import org.openqa.selenium.WebDriver;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import Config.ConfigFileReader;
import Utilities.EmailHelper;
import Utilities.GenericFunction;
import Utilities.Details;

public class AutomationTest {

	String pp32Url;
	String gmailUrl;
	String browserType;
	GenericFunction generic;
	WebDriver driver;
	Details details;
	EmailHelper emailHelper;

	@BeforeTest
	public void beforeTest() {
		pp32Url = ConfigFileReader.getConfigValue("pp32Url");
		gmailUrl = ConfigFileReader.getConfigValue("gmailUrl");
		browserType = ConfigFileReader.getConfigValue("browser");
		generic = new GenericFunction(driver);
		driver = generic.StartDriver(browserType);
		driver.manage().window().maximize();
		details =new Details(driver, generic);
		emailHelper=new EmailHelper(driver, generic);
	}

	@Test
	public void test1() {
		driver.get(pp32Url);
		details.startFilling();
		driver.get(gmailUrl);
		emailHelper.verifyEmail();
	}

}
